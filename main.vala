bool on_incoming_connection(SocketConnection conn) {
  process_request.begin(conn);
  return true;
}
size_t BUFFER_LENGTH = 1024 * 4;
async void process_request(SocketConnection conn) {
  try {
    var dis = new DataInputStream(conn.input_stream);
    var dos = new DataOutputStream(conn.output_stream);
    string req = yield dis.read_line_async(Priority.HIGH_IDLE);
    string path = "./"+safedir(req.split(" ")[1]);
    if (path == ".//") {
      path = "./index.html";
    }
    stderr.printf("%s\n", path);
    if (isfile(path)) {
      FileStream stream = FileStream.open(path, "r");
      // get file size:
      stream.seek(0, FileSeek.END);
      long size = stream.tell();
      stream.rewind();

      dos.put_string("HTTP/1.1 200 OK\n");
      dos.put_string("Server: simple-httpd\n");
      if (endswith(path.down(), ".html")) {
        dos.put_string("Content-Type: text/html\n");
      }else if (endswith(path.down(), ".css")) {
        dos.put_string("Content-Type: text/css\n");
      } else {
        dos.put_string("Content-Type: text/plain\n");
      }
      dos.put_string("Content-Length: %l\n\n".printf(size));
      uint8[] buf = new uint8[BUFFER_LENGTH];
      size_t read_size = 0;
      size_t written = 0;
      while ((read_size = stream.read(buf)) != 0) {
        written = 0;
        while (written < read_size) {
          written += dos.write(buf[written: read_size]);
        }
        dos.flush();
      }
    } else if (isdir(path)) {
      dos.put_string("HTTP/1.1 200 OK\nContent-Type: text/html\n\n");
      dos.put_string("<html>\n<head>");
      dos.put_string("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n");
      dos.put_string("<title>Directory listing for " + path[1: ] + "</title>\n");
      dos.put_string("</head>\n<body>\n");
      dos.put_string("<h1>Directory listing for " + path[1: ] + "</h1>\n");
      dos.put_string("<hr>\n<ul>\n");
      dos.put_string("<li><a href=\"../\">..</a><br></li>\n");
      dos.flush();
      foreach(string f in listdir(path)) {
        string v = "";
        if (isdir(f)) {
          v = "/";
        }
        string ff = f.replace(">", "&gt;").replace("<", "&lt;") + v;
        dos.put_string("<li><a href=\"" + path + "/" + f + "\">" + ff + "</a><br></li>\n");
        dos.flush();
      }
      dos.put_string("</ul>\n<hr>\n");
      dos.put_string("</body>\n</html>");
    }

  } catch (Error e) {
    stderr.printf("%s\n", e.message);
  }
}
bool endswith(string data, string f) {
  if (data.length < f.length) {
    return false;
  }
  return data[data.length - f.length: ] == f;
}
string[] listdir(string path){
  string[] ret = {};
  string name;
  try{
      Dir dir = Dir.open(path+"/", 0);
      while ((name = dir.read_name ()) != null) {
          ret += name;
      }
   }catch(Error e){
      stderr.printf("%s\n", e.message);
      return {};
  }
  return ret;
}


void main(string[] args) {
  int port = 8000;
  if(args.length > 1){
    if (args[1] != "") {
        port = int.parse(args[1]);
    }
  }
  try {
    var srv = new SocketService();
    srv.add_inet_port((uint16)port, null);
    srv.incoming.connect(on_incoming_connection);
    srv.start();
    stderr.printf("Servering HTTP on 0.0.0.0 port %s\n".printf(port.to_string()),true,true);
    new MainLoop().run();
  } catch (Error e) {
    stderr.printf("%s\n", e.message);
  }
}

public bool isfile(string path) {
  return GLib.FileUtils.test(path, GLib.FileTest.IS_REGULAR) ||
    GLib.FileUtils.test(path, GLib.FileTest.IS_SYMLINK);
}

public bool isdir(string path){
  return GLib.FileUtils.test(path, GLib.FileTest.IS_DIR) &&
  ! GLib.FileUtils.test(path, GLib.FileTest.IS_SYMLINK);
}


public string safedir(string dir) {
  string ret = dir;
  ret = ret.replace("..", ".").replace("//", "/");
  return ret;
}
