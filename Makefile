CFLAGS = -O2 -s
all: clean build

clean:
	rm -rf build
build:
	mkdir -p build
	cd build && valac -C ../main.vala --pkg gio-2.0
	$(CC) -fPIC $(CFLAGS) -c build/main.c -o build/main.o `pkg-config --cflags gio-2.0`
	$(CC) -fPIC $(CFLAGS) build/main.o -o build/main `pkg-config --cflags --libs gio-2.0`

run:
	./build/main
